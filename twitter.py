"""
Author: Terence Liu
Email:  llebox@hotmail.com
"""



# Import the necessary modules
from twitter import *
from twitter.stream import *
import nltk
from nltk.corpus import stopwords
from openpyxl import load_workbook, Workbook
import xlsxwriter
from datetime import datetime, time
import io, os, errno, time, openpyxl, re, csv, MySQLdb, urllib2, datetime, time, urllib, json
import httplib
from urllib2 import *




# set up OAUTH
def connectTwitter():

    # Variables that contains the user credentials to access Twitter API 
    ACCESS_TOKEN = ""
    ACCESS_SECRET = ""
    CONSUMER_KEY = ""
    CONSUMER_SECRET = ""

    oa = OAuth(ACCESS_TOKEN, ACCESS_SECRET, CONSUMER_KEY, CONSUMER_SECRET)

    return oa



def twitterParseText(wbname,stopname,tweet_count,keyword):


    # connect to MySQL database & cursor
    # This version should work when ran under cmd...
    conn = MySQLdb.connect('localhost', 'ExpressJS2016', 'twitter123','test', charset ='utf8', use_unicode = True)
    cursor = conn.cursor()
    cursor.execute('SET NAMES utf8mb4')
    cursor.execute("SET CHARACTER SET utf8mb4")
    cursor.execute("SET character_set_connection=utf8mb4")


    # create workbook object and worksheet
    wb = load_workbook(filename = wbname)
    wb.save(wbname)
    wbnames = wb.get_sheet_names()
    ws = addSheets(wbnames,wbname)


    # Initiate the connection to Twitter Streaming API
    twitter_stream = TwitterStream(auth = connectTwitter())
    t = Twitter(auth = connectTwitter())

    # Print each tweet in the stream to the screen 
    # You don't have to set it to stop, but can continue running 
    # the Twitter API to collect data for days or even longer. 
    count = 0
    checkCreation = 0

    # check if the sheet is empty, if it has been filled go to the end of row
    if ws.max_row == 0:
        rowCount = 2
    else:
        rowCount = ws.max_row + 1 
    

    # get list of stop words
    stopwords = getStopWords(stopname)

    # store into array
    arr = []

    # Get a sample of the public data following through Twitter
    while True:

        try:
            
            # connection tryout
            iterator = twitter_stream.statuses.filter(track=keyword, language="en", retry="true", stall_warnings = 'true', filter_level = 'none')
            print "-- Successfully connected to TwitterStream! --"


            # loop through the tweets
            for tweet in iterator:

                # catch ValueError and IO problems within the loop..    
                try:

                    # restart the stream connection at every 2000 iteration
                    if tweet_count % 100 == 0:
                        tweet_count -= 1
                        time.sleep(1)
                        break

                    # if it gets timeout, continue..
                    if not tweet or tweet.get("timeout"):
                        continue

                    # if disconnects or hangup, just break from the for loop and re-issue connection
                    if tweet.get("disconnect") or tweet.get("hangup"):
                        print "WARNING Stream connection lost: %s" % (tweet)
                        break

                    # only proceed if iterator returns text
                    if tweet.get('text'):

                        # reopen the workbook with the corresponding worksheet, given that the workbook has been closed after wb.save
                        wb = load_workbook(filename = wbname)

                        # only create a new sheet at a new date
                        currTime = datetime.datetime.now()
                        
                        # only create new sheet at 0:00:00 am, only give 59 seconds reaction time..
                        if checkCreation == 0 and currTime.hour == 0 and currTime.minute == 0 and currTime.second >= 0 and currTime.second <= 59:
                            print "-- NEW DATE NEW SHEET CREATED! --"
                            ws = addSheets(wbnames,wbname)
                            wb = load_workbook(filename = wbname)
                            ws = wb.get_sheet_by_name(time.strftime("%Y-%m-%d"))
                            rowCount = ws.max_row + 1
                            checkCreation += 1

                        elif checkCreation >= 0 and currTime.hour == 0 and currTime.minute == 0 and currTime.second >= 0 and currTime.second <= 59:
                            ws = wb.get_sheet_by_name(time.strftime("%Y-%m-%d"))

                        elif checkCreation >= 0 and not (currTime.hour == 0 and currTime.minute == 0 and currTime.second >= 0 and currTime.second <= 59):
                            ws = wb.get_sheet_by_name(time.strftime("%Y-%m-%d"))
                            checkCreation = 0


                        # add JSON content into array
                        arr.append(json.dumps(tweet))
                    
                        # Read in one line of the file, convert it into a json object 
                        tweet = json.loads(arr[count])
  

                        # only considers non empty texts
                        if 'text' in tweet and not (tweet['text'] is None) and not (tweet['text'] == "") and tweet['retweeted'] is False: # only messages contains 'text' field is a tweet

                            # encode fix for unicode problems..
                            tweet['text'] = tweet['text'].encode('utf-8', errors='replace')
                            tweet['text'] = removeNonEnglishWords(tweet['text']).encode('utf-8', errors='replace')
                            #tweet['text'] = normalize(tweet['text'])
                            # extract tweet and apply tokenization with stop words
                            #parsedText = getTokenization(filterTweet(tweet['text']), stopwords)
                             
                            # save hashtags into an array, then convert into a string concatenated with commas
                            hashtags = []
                            for hashtag in tweet['entities']['hashtags']:
                                hashtag['text'] = removeNonEnglishWords(hashtag['text'])
                                hashtags.append(hashtag['text'])

                            datetimeStamp = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                            # print tweet info:
                            print "tweet #" + str(count + 1) + ":"
                            print "time: ", datetimeStamp
                            print tweet['text'] # content of the tweet
                            print "user: ", tweet['user']['name'] # id of user
                            print "hashtag: " + ','.join(hashtags)
                            

                            # prints to excel file
                            # converts hashtag array into string
                            ws.cell(column = 1, row = rowCount, value = tweet['id_str'])
                            ws.cell(column = 2, row = rowCount, value = tweet['user']['id'])
                            ws.cell(column = 3, row = rowCount, value = tweet['user']['name'])
                            ws.cell(column = 4, row = rowCount, value = tweet['text'])
                            #ws.cell(column = 3, row = rowCount, value = parsedText)
                            ws.cell(column = 10, row = rowCount, value = datetimeStamp)
                            ws.cell(column = 5, row = rowCount, value = ','.join(hashtags))


                            # if we are looking for Bitcoin tweets, we store their price at the time of the search
                            if re.search(r'\bbtc\b', tweet['text'].lower()) or re.search(r'\bbitcoin\b',tweet['text'].lower()) or re.search(r'\bbtcusd\b',tweet['text'].lower()):
                                #url = urlopen('https://api.coindesk.com/v1/bpi/currentprice.json').read()
                                url = urlopen('https://www.bitstamp.net/api/v2/ticker/btcusd/').read()
                                results = json.loads(url)
                                #price = results['bpi']['USD']['rate']
                                price = results['last']
                                volume = results['volume']
                                bid = results['bid']
                                ask = results['ask']
                                vwap = results['vwap']

                                # insert data into table in MySQL
                                ws.cell(column = 6, row = rowCount, value = price)
                                ws.cell(column = 7, row = rowCount, value = bid)
                                ws.cell(column = 8, row = rowCount, value = ask)
                                ws.cell(column = 9, row = rowCount, value = vwap)
                                ws.cell(column = 11, row = rowCount, value = volume)
                                cursor.execute("INSERT INTO bitcoin (tweetID, userID, userName, tweetText, hashtags, btcPrice, btcBid, btcAsk, btcVwap, btcVolume, timeStamp) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                                                (tweet['id_str'], tweet['user']['id'], tweet['user']['name'], tweet['text'],','.join(hashtags), price, bid, ask, vwap, volume, datetimeStamp))
                
                            """

                            else:
                                
                                # USDJPY:CUR for USDJPY
                                if re.search(r'\busd/jpy\b', tweet['text'].lower()) or re.search(r'\busd jpy\b', tweet['text'].lower()) or re.search(r'\busd\b', tweet['text'].lower()):
                                    price = str(bbgPrice("USDJPY:CUR"))

                                # only get these info during market hours
                                else:

                                    #if currTime.hour >= 7 and currTime.minute >= 0 and currTime.second >= 0 and currTime.hour <= 19:
                                        
                                    # FB:US for facebook
                                    if re.search(r'\bfb\b', tweet['text'].lower()) or re.search(r'\bfacebook\b', tweet['text'].lower()):
                                        price = str(bbgPrice("FB:US"))

                                    # AAPL:US for AAPL
                                    elif re.search(r'\baapl\b', tweet['text'].lower()) or re.search(r'\bapple\b', tweet['text'].lower()):
                                        price = str(bbgPrice("AAPL:US"))

                                    # GOOG:US for GOOGLE
                                    elif re.search(r'\bgoog\b', tweet['text'].lower()) or re.search(r'\bgoogle\b', tweet['text'].lower()):
                                        price = str(bbgPrice("GOOG:US"))

                                    # SPY:US for SPY
                                    elif re.search(r'\bspy\b', tweet['text'].lower()):
                                        price = str(bbgPrice("SPY:US"))
                                        
                                    # SPX:US for SPX
                                    elif re.search(r'\bspx\b', tweet['text'].lower()):
                                        price = str(bbgPrice("SPX:IND"))
                                
                                    else:
                                        price = ""


                                ws.cell(column = 6, row = rowCount, value = price)
                                cursor.execute("INSERT INTO stocks (tweetID, userID, userName, tweetText, hashtags, price, timeStamp) VALUES (%s,%s,%s,%s,%s,%s,%s)",
                                                (tweet['id_str'], tweet['user']['id'], tweet['user']['name'], tweet['text'],','.join(hashtags), price, datetimeStamp))
                                    

                            """

                            # save at each iteration of looping tweets, this will actually close the workbook
                            wb.save(wbname)

                            # save into database    
                            conn.commit()
                            print "price:  ", price
                            print "volume: ", volume
                            print ""


                except ValueError, err:
                    # read in a line is not in JSON format (sometimes error occured)
                    print str(err) + " time: " + str(datetime.datetime.now())
                    continue

                except IOError, err:
                    print str(err) + " time: " + str(datetime.datetime.now())
                    continue

                except IndexError as err:
                    print "list index out of range.."
                    continue


                # break..
                if tweet_count < 0:
                    break

                # counter iterations
                tweet_count -= 1
                rowCount += 1
                count +=1


        # error handles
        except(TwitterHTTPError, httplib.BadStatusLine, URLError, SSLError, socket.error) as e:
            print("WARNING: Stream connection lost, reconnecting in a sec... (%s: %s)" % (type(e), e))
            continue 

        except urllib2.HTTPError,err:
            print str(err.code) + " time: " + str(datetime.datetime.now())
            continue


    # Finalize MySQL database
    conn.commit()
    cursor.close()
    conn.close()

    # close workbook
    wb.save(wbname)


#Filter tweets
def filterTweet(result):

    #Convert to lower case
    tweet = tweet.lower()
    tweet = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','URL',tweet)
    tweet = re.sub('@[^\s]+','USER',tweet)
    tweet = re.sub('[\s]+', ' ', tweet)
    tweet = re.sub(r'#([^\s]+)', r'\1', tweet)
    tweet = tweet.strip('\'"')

    return tweet
#end


def normalize(tweet):
    """
    :param tweet: tweet text
    :return: normalized text according to: Alec Go (2009)'s Twitter Sentiment Classification using Distant Supervision
    """
    # http://stackoverflow.com/questions/2304632/regex-for-twitter-username
    tweet = re.sub('(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)', 'USER', tweet)
    tweet = tweet.replace('\\', '')
    tweet = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', 'URL', tweet)
    # result = re.sub('[^a-zA-Z]', ' ', result)
    tweet = tweet.lower().split()

    stops = set(stopwords.words("english"))
    tokens = [t for t in result if (not t in stops)]
    tokens = [t for t in tokens if "#" in t or t.isalnum()]

    return " ".join(tokens)

#extract stopwords list and save into a list
def getStopWords(file):

    #read the stopwords file and build a list
    stopWords = []
    stopWords.append('AT_USER')
    stopWords.append('URL')
    stopWords.append('rt')
    stopWords.append('user')

    fp = open(file, 'r')
    line = fp.readline()
    while line:
        word = line.strip()
        stopWords.append(word)
        line = fp.readline()
    fp.close()

    return stopWords
#end


#extract features from parsed tweets
def getTokenization(tweet,stopwords):
    
    tokens = []
    words = tweet.split()

    for w in words:
        w = w.strip('\'"?,.')
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", w)
        if(w in stopwords or val is None):
            continue
        else:
            tokens.append(w.lower())

    #return as string
    return " ".join(tokens)
#end


# fix encoding problems from twitterStream, remove non-utf-8 characters
def removeNonEnglishWords(tweet):

    newTweet = []

    for i in range(len(tweet)):
        if tweet[i] != '':
            chk = re.match(r'([a-zA-z0-9 \+\?\.\*\^\$\(\)\[\]\{\}\|\\/:;\'\"><,.#@!~`%&-_=])+$', tweet[i])
            if chk:
                newTweet.append(tweet[i])

    #return as string
    return "".join(newTweet)


def replaceHashtag(tweet):
    """takes as input a list which contains words in tweet and return list of words in tweet after replacement 
    #*** - > # """
    for i in range(len(tweet)):
        if tweet[i].startswith('#'):
            tweet[i] = tweet[i][1:].strip(specialChar)

    return tweet,token


def replaceNegation(tweet):
    """takes as input a list which contains words in tweet and return list of words in tweet after replacement of "not","no","n't","~"
       eg isn't -> negation 
       not -> negation """   

    for i in range(len(tweet)):
        word = tweet[i].lower().strip(specialChar)
        if (word == "no" or word == "not" or word.count("n't") > 0):
            tweet[i] = 'negation'

    return tweet


def expandNegation(tweet):
    """takes as input a list which contains words in tweet and return list of words in tweet after expanding of "n't" to "not"
       eg isn't -> is not """
    
    newTweet = []
    newToken = []

    for i in range(len(tweet)):

        word = tweet[i].lower().strip(specialChar)

        if word[-3:] == "n't":
            if word[-5:] == "can't":
                newTweet.append('can')
            else:
                newTweet.append(word[:-3])
            newTweet.append('not')
            newToken.append('V')
            newToken.append('R')
        else:
            newTweet.append(tweet[i])

    return newTweet


# only create worksheet if it does not already exist
# reverse loop to be faster, knowing the most recent file will be at the end
def addSheets(wbnames,wbname):

    wb = load_workbook(filename = wbname)
    check = 0
    for name in reversed(wbnames):
        if name == time.strftime("%Y-%m-%d"):
            check += 1
        continue

    # check if sheet already exists..
    if check == 0:
        print "Creating worksheet(" + time.strftime("%Y-%m-%d") + ")"
        ws = wb.create_sheet(title = time.strftime("%Y-%m-%d"))

        # set up the columns
        ws.cell(column = 1, row = 1, value = "USER_ID")
        ws.cell(column = 2, row = 1, value = "TEXT")
        ws.cell(column = 3, row = 1, value = "HASHTAGS")
        ws.cell(column = 4, row = 1, value = "TIME")
        ws.cell(column = 5, row = 1, value = "BTC_PRICE")

        # save the file, will be opened again while reading the tweet
        wb.save(filename = wbname)

    else:
        print ("worksheet(" + time.strftime("%Y-%m-%d") + ") already exists!")
        ws = wb.get_sheet_by_name(time.strftime("%Y-%m-%d"))

    return ws



# get currency price, USDJPY:CUR for USDJPY , CL1:COM for Crude oil and etc..
def bbgPrice(quoteName):

    base_url = 'http://www.bloomberg.com/quote/'
    content = urllib.urlopen(base_url + quoteName).read()
    anchor = '<div class="price">'
    startIndex = content.index('<div class="price">')
    content = content[startIndex + len(anchor) : startIndex + 45]
    content = content.replace(",","")
    m = re.findall('\d+.\d+', content)

    return m[0]



# create the excel file to hold on everything
def main():

    # create new workbook at all time......
    wbName = time.strftime("%Y-%m-%d-%H-%M-%S")+ ".xlsx"
    workbook = xlsxwriter.Workbook(wbName)
    workbook.close()

    
    # define new keywords
    keywords = ""
    #$AAPL, $AAPL Apple, $FB, $FB Facebook, $GOOG, $GOOG google,$SPY, $SPX
    # run through the codes
    # spaces = AND, commas = OR
    twitterParseText(wbName,"stopwords.txt", 100000002, keywords)


# Call main from scripts
if __name__ == '__main__':
    main()