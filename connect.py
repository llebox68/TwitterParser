import nltk
import MySQLdb, sys
from collections import Counter
import collections
from itertools import chain
from compiler.ast import flatten
import openpyxl
from openpyxl import load_workbook, Workbook

def getWordTokens(tweets):

    all_words = []
    for (words) in tweets:
      all_words.append(words)
    return all_words


def getWordFeatures(wordlist):

    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features


def main():

    # connect to MySQL database & cursor
    conn = MySQLdb.connect('localhost', 'root', '','test',charset='utf8')
    cursor = conn.cursor()
    
    wb = load_workbook(filename = "features.xlsx")
    ws = wb.get_sheet_by_name("Sheet1")

    # store MySQL features into array
    try:
        cursor.execute("SELECT DISTINCT PARSED_TEXT from parser")
        iterate = cursor.fetchone()
        wordTokens = []
        featureArr = []

        # do this for each row
        while iterate is not None:
            for word in iterate:
                if len(word) > 1:
                    wordTokens.append(word.split())
            iterate = cursor.fetchone()

        #wordTokens = flatten(wordTokens)
        #print wordTokens
        #print wordTokens
        

        wordTokens = Counter(list(chain.from_iterable(wordTokens)))
        #word_features = getWordFeatures(wordTokens)

        count = 1
        for iterating, key in wordTokens.most_common(len(wordTokens)):
            print '%s: %d' % (iterating, key)
            ws.cell(column = 2, row = count, value = iterating)
            ws.cell(column = 3, row = count, value = key)
            #ws.cell(column = 3, row = count, value = wordTokens.count(iterating))
            count += 1

        wb.save("features.xlsx")    

        #print wordTokens
        cursor.close()
        conn.close()

    except MySQLdb.OperationalError, err:
        raise err

if __name__ == '__main__':
    main()